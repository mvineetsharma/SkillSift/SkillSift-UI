import { SkillSiftMCQ } from '@/types';
import { Demo } from '@/types';


export const MCQService = {
    getMCQ() {
        //console.log('In MCQService');
        return fetch('/demo/data/mcq.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) => 
                d.data as SkillSiftMCQ.MCQQuestion[]
            );
    },

    getMCQTopics() {
        //console.log('In MCQService');
        return fetch('/demo/data/topics.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) => 
                d.data as SkillSiftMCQ.MCQTpoic[]
            );
    },

    getMCQCategories() {
        //console.log('In MCQService');
        return fetch('/demo/data/categories.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) => 
                d.data as SkillSiftMCQ.MCQCategory[]
            );
    },

    getProducts() {
        return fetch('/demo/data/products.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) => d.data as Demo.Product[]);
    },

    getProductsWithOrdersSmall() {
        return fetch('/demo/data/products-orders-small.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) => d.data as Demo.Product[]);
    }
};
