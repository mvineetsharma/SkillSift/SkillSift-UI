import { SkillSiftUsers } from '@/types';

export const UserService = {
    getUsers() {
        //console.log('In MCQService');
        return fetch('/demo/data/users.json', { headers: { 'Cache-Control': 'no-cache' } })
            .then((res) => res.json())
            .then((d) =>
                d.data as SkillSiftUsers.User[]
            );
    }
}