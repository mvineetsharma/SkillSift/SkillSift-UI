export type LayoutType = 'list' | 'grid';

declare namespace SkillSiftMCQ {
    type MCQQuestion = {
        _id: { $oid: string };
        topic: string;
        question: string;
        options: MCQOptions[];
        answer: string;
        category: string;
        createDate: string;
        modifiedDate: string;
        isActive: boolean;
    };

    type MCQOptions = {
        key: string;
        value: string;
    };

    type MCQTpoic = {
        _id: {
            $oid: string;
        };
        topic: string;
        createDate: string;
        modifiedDate: string;
        modifiedBy: string;
        isActive: boolean;
    };

    type MCQCategory =
        {
            _id: {
                $oid: string;
            };
            category: string;
            createDate: string;
            modifiedDate: string;
            modifiedBy: string;
            isActive: boolean;
        };
}