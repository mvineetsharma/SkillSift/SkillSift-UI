export type LayoutType = 'list' | 'grid';

declare namespace SkillSiftUsers {
    type User = {
        _id: {
            $oid: string;
          };
          firstName: string;
          lastName: string;
          createDate: string; // Assuming ISO 8601 date format
          modifiedDate: string; // Assuming ISO 8601 date format
          email: string;
          isActive: boolean;
    };

    
}